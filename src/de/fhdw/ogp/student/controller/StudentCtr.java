package de.fhdw.ogp.student.controller;

import de.fhdw.ogp.student.model.*;
import de.fhdw.ogp.student.utils.Console;

public class StudentCtr {
    public static Student runNewDialogue() {
        Console.startDialogueSection();

        String name = Console.readString("Enter name");
        Location residence = LocationCtr.runNewDialogue();

        Subject[] subjectSet = Subject.values();
        String[] subjectSetStr = new String[subjectSet.length];
        int i = 0;
        for (Subject subject : subjectSet) {
            subjectSetStr[i++] = subject.toString();
        }

        String choice = Console.readChoice(subjectSetStr);
        Subject subject = Subject.parse(choice);

        Console.concludeDialogueSection();

        return new Student(name, residence, subject);
    }

    public static void runEditDialogue(Student student) {
        Console.startDialogueSection();

        if (student != null) {
            for (boolean goOn = true; goOn; ) {
                String choice = Console.readChoice("New name", "New residence", "Edit residence", "Exit");
                switch (choice) {
                    case "New name": {
                        student.setName(Console.readString("New name"));
                        break;
                    }
                    case "New residence": {
                        student.setResidence(LocationCtr.runNewDialogue());
                        break;
                    }
                    case "Edit residence": {
                        Location residence = student.getResidence();
                        LocationCtr.runEditDialogue(residence);
                        break;
                    }
                    case "Exit": {
                        goOn = false;
                        break;
                    }
                }
            }
        }

        Console.concludeDialogueSection();
    }

    public static void runMainDialogue() {
        Console.startDialogueSection();
        Student student = null;
        String choice = Console.readChoice("New Student", "Exit");
        switch (choice) {
            case "New Student": {
                student = runNewDialogue();
                break;
            }
            case "Exit": {
                break;
            }
        }
        Console.concludeDialogueSection();
    }
}
