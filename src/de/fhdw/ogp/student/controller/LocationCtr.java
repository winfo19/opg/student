package de.fhdw.ogp.student.controller;

import de.fhdw.ogp.student.model.Location;
import de.fhdw.ogp.student.utils.Console;

public class LocationCtr {

    public static Location runNewDialogue() {
        Console.startDialogueSection();
        String street = Console.readString("Enter street");
        int zip = Console.readInt("Enter zip", 10000, 99999);
        String city = Console.readString("Enter city");
        Console.concludeDialogueSection();
        return new Location(street, zip, city);
    }

    public static void runEditDialogue(Location location) {
        Console.startDialogueSection();

        if (location != null) {
            for (boolean goOn = true; goOn; ) {
                String choice = Console.readChoice("New city", "New street", "New zip", "Exit");

                Console.startDialogueSection();

                switch (choice) {
                    case "New city": {
                        location.setCity(Console.readString("New name"));
                        break;
                    }
                    case "New street": {
                        location.setStreet(Console.readString("New street"));
                        break;
                    }
                    case "New zip": {
                        location.setZip(Console.readInt("New zip", 10000, 99999));
                        break;
                    }
                    case "Exit": {
                        goOn = false;
                        break;
                    }
                }

                Console.concludeDialogueSection();
            }

            Console.concludeDialogueSection();
        }
    }

    public static void runMainDialogue() {
        Console.startDialogueSection();
        Location location = null;
        String choice = Console.readChoice("New Location", "Exit");
        switch (choice) {
            case "New Location": {
                location = runNewDialogue();
                break;
            }
            case "Exit": {
                break;
            }
        }
        Console.concludeDialogueSection();
    }
}
