package de.fhdw.ogp.student.controller;

import de.fhdw.ogp.student.model.Student;
import de.fhdw.ogp.student.model.StudentSet;
import de.fhdw.ogp.student.utils.Console;

public class StudentSetCtr {
    public static void printStudentSet(StudentSet studentSet) {
        if (studentSet.getNo() == 0) {
            Console.printlnMessage("No students available");
        } else {
            Console.printlnMessage("Students actually available");
            for (Student student : studentSet.getAll()) {
                Console.printlnMessage("- " + student);
            }
        }
    }

    public static void runMainDialogue() {
        Console.startDialogueSection();

        StudentSet studentSet = new StudentSet();
        int id;
        for (boolean goOn = true; goOn; ) {
            printStudentSet(studentSet);
            String choice = Console.readChoice("Add student", "Edit student", "Remove student", "Exit");

            Console.startDialogueSection();

            switch (choice) {
                case "Add student": {
                    studentSet.add(StudentCtr.runNewDialogue());
                    break;
                }
                case "Edit student": {
                    id = Console.readInt(
                            "Enter id from " + Student.FIRST_ID + " to " + Student.LAST_ID,
                            Student.FIRST_ID,
                            Student.LAST_ID
                    );
                    Student student = studentSet.getStudent(id);
                    StudentCtr.runEditDialogue(student);
                    break;
                }
                case "Remove student": {
                    id = Console.readInt(
                            "Enter id from " + Student.FIRST_ID + " to " + Student.LAST_ID,
                            Student.FIRST_ID,
                            Student.LAST_ID
                    );
                    studentSet.remove(id);
                    break;
                }
                case "Exit": {
                    goOn = false;
                    break;
                }
            }

            Console.concludeDialogueSection();
        }

        Console.concludeDialogueSection();
    }
}
