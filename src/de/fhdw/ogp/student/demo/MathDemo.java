package de.fhdw.ogp.student.demo;

public class MathDemo {
    public static void main(String[] args) {
        // Absolutwerte, Max, Min
        System.out.println("sqrt(pow(-7 ,2)): " + Math.sqrt(Math.pow(-7, 2)));
        System.out.println("abs(-7): " + Math.abs(-7));
        System.out.println("max(1999, 9): " + Math.max(1999, 9));
        System.out.println("min(1999, 9): " + Math.min(1999, 9));

        // exp
        System.out.println("exp(0): " + Math.exp(0));
        System.out.println("exp(1): " + Math.exp(1));
        System.out.println("exp(-1000000): " + Math.exp(-1000000));

        // log
        System.out.println("log(1): " + Math.log(1));
        System.out.println("log(0): " + Math.log(0));

        for (int i = 0; i < 10; i++) {
            System.out.println("log(exp(" + i + ")): " + Math.log(Math.exp(i)));
        }

        // sqrt
        int a = 10;
        int b = 8;
        System.out.println("sqrt(" + a + "² + " + b + "²): " + Math.sqrt(a * a + b * b));

        // sin, cos, tan
        System.out.println("sin(900044): " + Math.sin(900044));
        System.out.println("cos(900044): " + Math.cos(900044));
        System.out.println("tan(900044): " + Math.tan(900044));
    }
}
