package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.Subject;

@SuppressWarnings("ConstantConditions")
public class TestEnums {
    public static void main(String[] args) {
        Subject subject1 = Subject.APPLIED_COMPUTER_SCIENCE;
        Subject subject2 = Subject.GENERAL_MANAGEMENT;

        System.out.println("Not equal: " + subject1.equals(subject2));

        System.out.println("Nummer: " + subject1.ordinal());

        Subject[] subjectSet = Subject.values();
        for (Subject subject : subjectSet) {
            System.out.println("Nummer: "
                               + subject.ordinal()
                               + " Name: "
                               + subject.name()
                               + " Sprechender name: "
                               + subject);
        }
    }
}
