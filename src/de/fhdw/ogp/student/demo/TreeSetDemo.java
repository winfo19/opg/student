package de.fhdw.ogp.student.demo;

import java.util.Iterator;
import java.util.TreeSet;

public class TreeSetDemo {
    public static void main(String[] args) {
        TreeSet<Double> treeSet = new TreeSet<>();
        treeSet.add(1d);
        treeSet.add(10d);
        treeSet.add(9d);
        treeSet.add(-1d);
        treeSet.add(0d);

        Iterator<Double> i = treeSet.iterator();
        //noinspection WhileLoopReplaceableByForEach
        while (i.hasNext()) {
            System.out.println(i.next());
        }

        treeSet.remove(1d);
        System.out.println(treeSet);
    }
}
