package de.fhdw.ogp.student.demo;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class DateTimeDemo {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(Locale.GERMANY);

        LocalDate today = LocalDate.now();
        System.out.println("today: " + today.format(formatter));

        LocalDate someWeeksBefore = today.minusWeeks(5);
        System.out.println("Some weeks before: " + someWeeksBefore.format(formatter));

        LocalDateTime now = LocalDateTime.now();
        System.out.println("now: " + now.format(formatter));

        LocalDateTime in3Hours = now.plusHours(3);
        System.out.println("In 3 Hours: " + in3Hours.format(formatter));

        Year thisYear = Year.now();
        System.out.println("This year: " + thisYear);

        Year anotherYear = thisYear.minusYears(2020);
        for (int i = 0; i < 2000; i++) {
            if (anotherYear.isLeap()) {
                System.out.println("Is leap year: " + anotherYear);
            }
            anotherYear = anotherYear.plusYears(1);
        }
    }
}
