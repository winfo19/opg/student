package de.fhdw.ogp.student.demo;

public class StringDemo {
    @SuppressWarnings({"StringEquality", "StringOperationCanBeSimplified", "ConstantConditions"})
    public static void main(String[] args) {
        String string1 = "abcde";
        String string2 = "abcde";
        String string3 = new String("abcde");
        String string4 = null;
        String string5 = "abcd";

        System.out.println("Compare string1 and string2: " + (string1 == string2));
        System.out.println("Compare string1 and string3: " + (string1 == string3));

        System.out.println("string1 address: " + Integer.toHexString(System.identityHashCode(string1)));
        System.out.println("string2 address: " + Integer.toHexString(System.identityHashCode(string2)));
        System.out.println("string3 address: " + Integer.toHexString(System.identityHashCode(string3)));
        System.out.println("string5 address: " + Integer.toHexString(System.identityHashCode(string5)));

        System.out.println("string1 length: " + string1.length());
        System.out.println("string1 contains string5" + string1.contains(string5));
        string1 = string1.toUpperCase();

        System.out.println("string1 upper: " + string1);
        System.out.println("string1 address: " + Integer.toHexString(System.identityHashCode(string1)));
        System.out.println("string2 address: " + Integer.toHexString(System.identityHashCode(string2)));

        String vorname = "Hans";
        System.out.println("String concat: " + vorname + " Peter");

        byte[] bArr = {81, 101, 101, 107, 115};
        String s = new String(bArr);
        System.out.println("String from byte array: " + s);

        char[] charArr = {'H', 'e', 'l', 'l', 'o'};
        String s2 = new String(charArr, 1, 3);
        System.out.println("String from char array: " + s2);
    }
}
