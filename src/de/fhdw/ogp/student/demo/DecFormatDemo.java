package de.fhdw.ogp.student.demo;

import java.text.DecimalFormat;

public class DecFormatDemo {
    public static void main(String[] args) {
        String pattern = "###,###.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);

        String formatted = decimalFormat.format(123456788.5678);
        System.out.println("Zahl: " + formatted);

        String pattern2 = "0%";
        DecimalFormat decimalFormat2 = new DecimalFormat(pattern2);
        System.out.println("Percent: " + decimalFormat2.format(1.12));
    }
}
