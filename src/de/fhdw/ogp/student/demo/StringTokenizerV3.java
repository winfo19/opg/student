package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringTokenizer;

public class StringTokenizerV3 {
    public static void main(String[] args) throws IOException {
        StudentSet studentSet = new StudentSet();
        BufferedReader bReader = Files.newBufferedReader(Paths.get("test.csv"));
        StringTokenizer st;
        String line;
        while ((line = bReader.readLine()) != null) {
            st = new StringTokenizer(line, ";");
            studentSet.add(new Student(
                    st.nextToken(),
                    new Location(st.nextToken(), Integer.parseInt(st.nextToken()), st.nextToken()),
                    Subject.parse(st.nextToken())
            ));
        }

        for (Student student : studentSet.getAll()) {
            System.out.println(" - " + student);
        }

        bReader.close();
    }
}
