package de.fhdw.ogp.student.demo;

public class StringBufferDemo {
    public static void main(String[] args) {
        StringBuffer stringBuffer = new StringBuffer("Text");

        // Append
        String string = " appended text";
        System.out.println("Buffer: " + stringBuffer);
        stringBuffer.append(string);
        System.out.println("Buffer appended: " + stringBuffer);
        System.out.println("stringBuffer address: " + Integer.toHexString(System.identityHashCode(stringBuffer)));
        System.out.println("string address: " + Integer.toHexString(System.identityHashCode(string)));

        // Delete
        stringBuffer.delete(stringBuffer.length() - string.length(), stringBuffer.length());
        System.out.println("Buffer deleted: " + stringBuffer);
        System.out.println("stringBuffer address: " + Integer.toHexString(System.identityHashCode(stringBuffer)));
        string = "new";
        System.out.println("string address: " + Integer.toHexString(System.identityHashCode(string)));
    }
}
