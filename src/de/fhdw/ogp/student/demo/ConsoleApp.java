package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.utils.Console;

public class ConsoleApp {
    public static void main(String[] args) {
        Console.printlnMessage("Mein Name ist [removed]");
        Console.printMessage("Meine Frau heißt:");
        Console.printlnMessage("[removed]");

        Console.printlnErrorMessage("Das war eine blöde Eingabe!!!!!");

        Console.startDialogueSection();
        int number = Console.readInt("Bitte Zahl eingeben");
        Console.printlnMessage("Die eingegebene Zahl lautet: " + number);
        Console.concludeDialogueSection();

        Console.startDialogueSection();
        String string = Console.readString("Bitte einen Test eingeben");
        Console.printlnMessage("Test: " + string);
        Console.concludeDialogueSection();

        Console.startDialogueSection();
        String choice = Console.readChoice("Wahl 1", "Wahl 2", "Wahl 3", "Wahl 4");
        Console.printlnMessage("Die eingelesene Wahl war: " + choice);
        Console.concludeDialogueSection();
    }
}
