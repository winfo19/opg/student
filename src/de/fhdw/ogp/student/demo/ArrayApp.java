package de.fhdw.ogp.student.demo;


import java.util.Arrays;

@SuppressWarnings({"ForLoopReplaceableByForEach", "UnnecessaryLocalVariable", "ConfusingArgumentToVarargsMethod"})
public class ArrayApp {
    private static void testLastVar(int j, String... strings) {
        if (strings == null) {
            System.out.println("Null list passed");
            return;
        } else {
            System.out.println("Length of variable parameters: " + strings.length);
        }
        System.out.println(Arrays.toString(strings));
    }

    public static void main(String[] args) {
        int[] noArray = null;
        int[] intArray = new int[4];
        int[] primes = {1, 3, 5, 7, 11, 13};

        // Referenzen
        System.out.println("Before 2 edit: " + Arrays.toString(intArray));
        int[] intArray2 = intArray;
        intArray2[3] = 100;
        System.out.println("After 2 edit: " + Arrays.toString(intArray));

        // Access
        intArray[0] = 1;
        intArray[intArray.length - 1] = 100;

        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = i;
        }

        // .clone()
        int[] intArray3 = intArray.clone();

        // print
        System.out.println(Arrays.toString(intArray));
        System.out.println(Arrays.toString(intArray3));

        // foreach
        System.out.println("[");
        for (int value : primes) {
            System.out.println("  " + value + ",");
        }
        System.out.println("]");

        // matrix
        int[][] zahlenArr = {{1, 2, 3, 4}, {6, 3}, {0}, {8, 9}};
        for (int i = 0; i < zahlenArr.length; i++) {
            for (int j = 0; j < zahlenArr[i].length; j++) {
                System.out.print(zahlenArr[i][j] + "; ");
            }
        }
        System.out.println();

        System.out.println(Arrays.deepToString(zahlenArr));

        // Variable Parameter
        testLastVar(9, "Hier", "kommen", "viele", "Strings");
        testLastVar(9, "kommen");
        testLastVar(0, null);
    }
}
