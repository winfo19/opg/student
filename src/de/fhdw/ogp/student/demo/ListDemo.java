package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.List;

public class ListDemo {
    public static void main(String[] args) {
        List<String> stringList = new List<>();
        System.out.println(stringList);
        System.out.println("Length: " + stringList.getLength());
        stringList.add("Peter");
        stringList.add("und");
        stringList.add("der");
        stringList.add("Wolf");
        stringList.add("ist ein russisches Märchen");
        System.out.println(stringList);
        System.out.println("Length: " + stringList.getLength());
    }
}
