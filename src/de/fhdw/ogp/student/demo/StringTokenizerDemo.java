package de.fhdw.ogp.student.demo;

import java.util.StringTokenizer;

public class StringTokenizerDemo {
    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer("Das ist ein sinnvoller Text.", "i");
        while (st.hasMoreTokens()) {
            System.out.println("Tokens left: " + st.countTokens());
            System.out.println(st.nextToken());
        }
    }
}
