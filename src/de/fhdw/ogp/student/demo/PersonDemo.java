package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.*;

public class PersonDemo {
    @SuppressWarnings({"UnusedAssignment", "unused", "ConstantConditions"})
    public static void main(String[] args) {
        Person person = new Person("Hans Wurst", new Location("Street", 99999, "City"));
        Lecturer doz1 = new Lecturer("Hans Wurst",
                new Location("Street", 99999, "City"),
                Department.COMPUTE_SCIENCE,
                Role.LECTURER
        );
        System.out.println(doz1);

        Student student = new Student("test", new Location("str", 11111, "abc"), Subject.APPLIED_COMPUTER_SCIENCE);
        person = student; // Polymorphie
        System.out.println(student);

        // Type check
        if (person instanceof Student) {
            student = (Student) person; // Cast
        }
    }
}
