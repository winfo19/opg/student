package de.fhdw.ogp.student.demo;

import java.math.BigDecimal;
import java.math.BigInteger;

public class BigIntDemo {
    public static void main(String[] args) {
        BigInteger factor1 = new BigInteger("12345461235356757345234756234524765643643645366546");
        BigInteger factor2 = new BigInteger("32145483873490857234895962384539150942357340578324");

        BigInteger product = factor1.multiply(factor2);

        System.out.println("factor1 * factor2: " + product);

        System.out.println("product²: " + product.pow(2));

        BigDecimal factor3 = new BigDecimal("90237453456.2340378452593476");
        BigDecimal factor4 = new BigDecimal("32459034086345.2923746");

        System.out.println("factor3 * factor4: " + factor3.multiply(factor4));
    }
}
