package de.fhdw.ogp.student.demo;

import java.util.ArrayList;

@SuppressWarnings({"ConstantConditions", "UnnecessaryBoxing", "MismatchedQueryAndUpdateOfCollection"})
public class WrapperDemo {
    public static void main(String[] args) {
        int i = 123;
        Integer j = Integer.valueOf(i);

        String s = "10";
        int x = Integer.parseInt(s);

        Integer wrapperInt = Integer.valueOf(5); // Boxing
        System.out.println(wrapperInt.intValue()); // Unboxing
        System.out.println(wrapperInt.equals(5)); // Autoboxing

        // Generic
        ArrayList<Integer> numberList = new ArrayList<>();
        numberList.add(Integer.valueOf(100));
        numberList.add(10);
    }
}
