package de.fhdw.ogp.student.demo;

import java.util.LinkedList;

public class CollectionDemo {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        System.out.println(list);
        list.add("Peter");
        System.out.println(list);
        list.add("und");
        System.out.println(list);
        list.add("der");
        System.out.println(list);
        list.add("Wolf");
        System.out.println(list);
        list.add(1, "Pan");
        System.out.println(list);
        list.remove("Pan");
        System.out.println(list);
        System.out.println(list.get(list.size() - 1));
    }
}
