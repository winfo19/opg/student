package de.fhdw.ogp.student.demo;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetDemo {
    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("Hello");
        hashSet.add("beautiful");
        hashSet.add("World");

        System.out.println("Length: " + hashSet.size());

        hashSet.add("World");

        System.out.println(hashSet);
        System.out.println(hashSet.contains("World"));

        Iterator<String> i = hashSet.iterator();

        //noinspection WhileLoopReplaceableByForEach
        while (i.hasNext()) {
            System.out.println(i.next());
        }

        hashSet.remove("Hello");

        System.out.println(hashSet);
    }
}
