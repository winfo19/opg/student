package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StringTokenizerV2 {
    public static void main(String[] args) throws IOException {

        StudentSet studentSet = new StudentSet();
        BufferedReader bReader = Files.newBufferedReader(Paths.get("test.csv"));
        String line;
        int plz;
        Subject subject;
        while ((line = bReader.readLine()) != null) {
            String[] studentInfo = line.split(";");
            plz = Integer.parseInt(studentInfo[2]);
            subject = Subject.parse(studentInfo[4]);

            studentSet.add(new Student(studentInfo[0], new Location(studentInfo[1], plz, studentInfo[3]), subject));
        }

        for (Student student : studentSet.getAll()) {
            System.out.println(" - " + student);
        }

        bReader.close();
    }
}
