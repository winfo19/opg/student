package de.fhdw.ogp.student.demo;

import de.fhdw.ogp.student.model.*;

public class PairDemo {
    public static void main(String[] args) {
        Pair<String> stringPair = new Pair<>("Hans", "Franz");
        System.out.println(stringPair);
        stringPair.setSecond("Carl");
        System.out.println(stringPair);
        stringPair.swap();
        System.out.println(stringPair);

        Pair<Person> personPair = new Pair<>(new Person("Helga", null), new Person("Fred", null));
        System.out.println(personPair);
        personPair.swap();
        System.out.println(personPair);

        Pair<Role> rolePair = new Pair<>(Role.DIRECTOR, Role.PROFESSOR);
        System.out.println(rolePair);
        rolePair.swap();
        System.out.println(rolePair);

        Pair<Integer> intPair = new Pair<>(1, 2);

        Pair<Sizable> sizablePair = new Pair<>(new Rectangle(1, 2), new Circle(5));

        int[] intArray1 = {1, 2, 3};
        int[] intArray2 = {4, 5, 6};
        Pair<int[]> intArrayPair = new Pair<>(intArray1, intArray2);
        System.out.println(intArrayPair);
    }
}
