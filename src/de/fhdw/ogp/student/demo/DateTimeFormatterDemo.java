package de.fhdw.ogp.student.demo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterDemo {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("ISO: " + localDateTime);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("EEEE 'der' dd.MMMM.yyyy HH:mm:ss");
        System.out.println("Formatted: " + dateTimeFormatter.format(localDateTime));
    }
}
