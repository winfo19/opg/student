package de.fhdw.ogp.student.model;

public enum Role {
    NO_ROLE("No Role"), PROFESSOR("Professor"), DIRECTOR("Director"), LECTURER("Lecturer");

    String string;

    Role(String string) {
        this.string = string;
    }

    public static String[] strings() {
        Role[] roles = Role.values();
        String[] result = new String[roles.length];

        for (Role role : roles) {
            result[role.ordinal()] = role.toString();
        }

        return result;
    }

    public static Role parse(String string) {
        for (Role role : Role.values()) {
            if (role.toString().equals(string)) {
                return role;
            }
        }

        return NO_ROLE;
    }

    @Override
    public String toString() {
        return string;
    }
}
