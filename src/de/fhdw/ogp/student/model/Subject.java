package de.fhdw.ogp.student.model;

public enum Subject {
    NO_SUBJECT("No Subject"),
    APPLIED_COMPUTER_SCIENCE("Applied Computer Science"),
    BUSINESS_COMPUTER_SCIENCE("Business Computer Science"),
    IT_MANAGEMENT_AND_INFORMATION_SYSTEMS("IT Management and Information System"),
    GENERAL_MANAGEMENT("General Management");

    private final String name;

    Subject(String name) {
        this.name = name;
    }

    public static Subject subject(int i) {
        int j = 0;
        for (Subject subject : Subject.values()) {
            if (i == j) {
                return subject;
            }
            j++;
        }
        return Subject.NO_SUBJECT;
    }

    public static Subject parse(String choice) {
        Subject subject;

        switch (choice) {
            case "Applied Computer Science": {
                subject = Subject.APPLIED_COMPUTER_SCIENCE;
                break;
            }
            case "Business Computer Science": {
                subject = Subject.BUSINESS_COMPUTER_SCIENCE;
                break;
            }
            case "IT Management and Information System": {
                subject = Subject.IT_MANAGEMENT_AND_INFORMATION_SYSTEMS;
                break;
            }
            case "General Management": {
                subject = Subject.GENERAL_MANAGEMENT;
                break;
            }
            default: {
                subject = Subject.NO_SUBJECT;
                break;
            }
        }

        return subject;
    }

    @Override
    public String toString() {
        return name;
    }
}
