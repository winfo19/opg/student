package de.fhdw.ogp.student.model;

public class Lecturer extends Person {
    private Department department;
    private Role       role;

    public Lecturer(String name, Location residence, Department department, Role role) {
        super(name, residence);

        this.department = department;
        this.role = role;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + "Lecturer{" + "department=" + department + ", role=" + role + '}';
    }
}
