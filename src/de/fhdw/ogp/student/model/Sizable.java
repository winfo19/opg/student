package de.fhdw.ogp.student.model;

public interface Sizable {
    double getSize();
}
