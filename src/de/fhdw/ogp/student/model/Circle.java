package de.fhdw.ogp.student.model;

public class Circle implements Sizable {
    int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double getSize() {
        return Math.PI * radius * radius;
    }
}
