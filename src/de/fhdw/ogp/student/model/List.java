package de.fhdw.ogp.student.model;

public class List<T> {
    private Node first;
    private Node last;

    public List() {}

    public void add(T object) {
        Node node = new Node(object, null);

        if (first == null) {
            first = node;
        } else {
            last.setNext(node);
        }

        last = node;
    }

    public int getLength() {
        int count = 0;
        for (Node elem = first; elem != null; count++) {
            elem = elem.getNext();
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("List[");
        for (Node elem = first; elem != null; ) {
            str.append(elem);
            if (!elem.isLast()) {
                str.append(", ");
            }
            elem = elem.getNext();
        }
        str.append("]");
        return str.toString();
    }

    private class Node {
        T    object;
        Node next;

        public Node(T object, Node next) {
            this.object = object;
            this.next = next;
        }

        public T getObject() {
            return object;
        }

        public void setObject(T object) {
            this.object = object;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public boolean isLast() {
            return next == null;
        }

        @Override
        public String toString() {
            return object.toString();
        }
    }
}
