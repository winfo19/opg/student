package de.fhdw.ogp.student.model;

public class Student extends Person {
    // Attribute
    public static final int FIRST_ID = 100001;
    public static final int LAST_ID  = 999999;

    private static int     nextId = FIRST_ID;
    private        int     id;
    private        Subject subject;

    // Konstruktoren
    public Student(String name, Location residence, Subject subject) {
        super(name, residence);

        this.subject = subject;
        this.id = getNextId();
    }

    public Student(String name, int id, Location residence) {
        super(name, residence);

        if (id >= FIRST_ID && id <= LAST_ID) {
            this.id = id;
        } else {
            this.id = 0;
        }
    }

    // Methoden
    public static int getNumberOfAvailableIds() {
        return LAST_ID - nextId;
    }

    public static int getNumberOfAssignedIds() {
        return nextId - FIRST_ID;
    }

    private static int getNextId() {
        if (nextId <= LAST_ID) {
            return nextId++;
        }
        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String firstName, String lastName) {
        setName(firstName + " " + lastName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Student student = (Student) o;

        return id == student.id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + "Student{" + "id=" + id + ", subject=" + subject + '}';
    }
}
