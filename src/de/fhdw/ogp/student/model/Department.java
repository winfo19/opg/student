package de.fhdw.ogp.student.model;

public enum Department {
    NO_DEPARTMENT("No Department"), BUSINESS_SCIENCE("Business Science"), COMPUTE_SCIENCE("Computer Science");

    String string;

    Department(String string) {
        this.string = string;
    }

    public static String[] strings() {
        Department[] departments = Department.values();
        String[] result = new String[departments.length];

        for (Department department : departments) {
            result[department.ordinal()] = department.toString();
        }

        return result;
    }

    public static Department parse(String string) {
        for (Department department : Department.values()) {
            if (department.toString().equals(string)) {
                return department;
            }
        }

        return NO_DEPARTMENT;
    }

    @Override
    public String toString() {
        return string;
    }
}
