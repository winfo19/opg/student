package de.fhdw.ogp.student.model;

@SuppressWarnings({"FieldMayBeFinal", "RedundantIfStatement"})
public class StudentSet {
    private Student[] studentArray;

    public StudentSet() {
        studentArray = new Student[Student.LAST_ID];
    }

    public void add(Student student) {
        studentArray[student.getId()] = student;
    }

    public Student getStudent(int id) {
        return isPresent(id) ? studentArray[id] : null;
    }

    public void remove(int id) {
        if (isPresent(id)) {
            studentArray[id] = null;
        }
    }

    public int getNo() {
        int count = 0;
        for (Student student : studentArray) {
            if (student != null) {
                count++;
            }
        }
        return count;
    }

    public Student[] getAll() {
        Student[] result = new Student[this.getNo()];
        int index = 0;

        for (Student student : studentArray) {
            if (student != null) {
                result[index++] = student;
            }
        }

        return result;
    }

    private boolean isPresent(int id) {
        if (id >= Student.FIRST_ID && id <= Student.FIRST_ID + Student.getNumberOfAssignedIds()) {
            return true;
        } else {
            return false;
        }
    }
}
