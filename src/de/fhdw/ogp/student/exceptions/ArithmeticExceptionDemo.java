package de.fhdw.ogp.student.exceptions;

public class ArithmeticExceptionDemo {
    public static void main(String[] args) {
        try {
            int a = 7;
            int b = 0;

            //noinspection ConstantConditions
            if (b == 0) {
                throw new DivisionByZeroException("b may not have the value 0");
            }

            int c = a / b;

            System.out.println("c = " + c);
        } catch (DivisionByZeroException e) {
            e.printStackTrace();
        }
    }
}
