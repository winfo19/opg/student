package de.fhdw.ogp.student.exceptions;

public class DivisionByZeroException extends Exception {
    public DivisionByZeroException() {
        super();
    }

    public DivisionByZeroException(String message) {
        super(message);
    }
}
