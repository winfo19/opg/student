package de.fhdw.ogp.student.apps;

import de.fhdw.ogp.student.controller.LocationCtr;
import de.fhdw.ogp.student.utils.Console;

public class LocationApp {
    public static void main(String[] args) {
        Console.printlnMessage("Location App starts");
        LocationCtr.runMainDialogue();
        Console.printlnMessage("Location App terminates");
    }
}
