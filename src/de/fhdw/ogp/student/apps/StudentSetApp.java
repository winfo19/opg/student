package de.fhdw.ogp.student.apps;

import de.fhdw.ogp.student.controller.StudentSetCtr;
import de.fhdw.ogp.student.utils.Console;

public class StudentSetApp {
    public static void main(String[] args) {
        Console.printlnMessage("StudentSet App starts");
        StudentSetCtr.runMainDialogue();
        Console.printlnMessage("StudentSet App terminates");
    }
}
