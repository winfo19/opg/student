package de.fhdw.ogp.student.apps;

import de.fhdw.ogp.student.controller.StudentCtr;
import de.fhdw.ogp.student.model.*;
import de.fhdw.ogp.student.utils.Console;

public class StudentApp {
    public static void main(String[] args) {
        // Location
        Location residence = new Location("Fürstenallee", 33106, "Paderborn");
        // Construct
        Student student = new Student("Hans Müller", 10002, residence);
        System.out.println("ID: " + student.getId());
        System.out.println("Name: " + student.getName());
        // Overload
        student.setName("Peter", "Müller");
        System.out.println("Neuer name: " + student.getName());

        // .toString()
        System.out.println("To string: " + student);

        // .equals()
        Student student2 = new Student("Hans Wurst", 10001, residence);

        System.out.println("Sind nicht gleich: " + student.equals(student2));

        Student student3 = new Student("Dieter", 10001, residence);
        System.out.println("Sind gleich: " + student2.equals(student3));

        // Ohne ID
        Student student4 = new Student("Ursula", residence, Subject.NO_SUBJECT);
        System.out.println(student4);
        Student student5 = new Student("Hildegard", residence, Subject.NO_SUBJECT);
        System.out.println(student5);

        // static Methoden
        System.out.println("Verfügbare ids: " + Student.getNumberOfAvailableIds());
        System.out.println("Vergebene ids: " + Student.getNumberOfAssignedIds());

        // Math
        System.out.println(Math.E);
        System.out.println(Math.PI);

        // Controller
        Console.printlnMessage("Student App starts");
        StudentCtr.runMainDialogue();
        Console.printlnMessage("Student App terminates");
    }
}
